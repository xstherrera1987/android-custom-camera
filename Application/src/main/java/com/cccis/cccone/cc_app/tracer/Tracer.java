package com.cccis.cccone.cc_app.tracer;

import android.util.Log;

public class Tracer {
	public static final String APP_TAG = "CustomCamera";

	public static void traceVerbose(String message) {
		Log.v(APP_TAG, message);
	}

	public static void traceDebug(String message) {
		Log.d(APP_TAG, message);
	}

	public static void traceWarning(String message) {
		Log.w(APP_TAG, message);
	}

	public static void traceError(String message) {
		Log.e(APP_TAG, message);
	}
}
