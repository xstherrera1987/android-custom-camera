package com.cccis.cccone.cc_app.views.camera;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cccis.cccone.cc_app.R;
import com.cccis.cccone.cc_app.views.misc.AutoFitTextureView;

public class CameraView extends RelativeLayout implements View.OnClickListener {

	public Button pictureBtn;
	public ImageButton infoBtn;

	/**
	 * An {@link AutoFitTextureView} for camera preview.
	 */
	public AutoFitTextureView textureView;

	private CameraListener listener;

	public CameraView(Context context) {
		super(context);
		init();
	}

	public CameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public CameraView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	private void init() {
		LayoutInflater.from(getContext()).inflate(R.layout.camera_view, this);

		pictureBtn = (Button) findViewById(R.id.picture);
		infoBtn = (ImageButton) findViewById(R.id.info);
		textureView = (AutoFitTextureView) findViewById(R.id.texture);

		pictureBtn.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
	}

	@Override protected void onFinishInflate() {
		super.onFinishInflate();
	}

	public void setListener(CameraListener listener) {
		this.listener = listener;
	}

	@Override public void onClick(View v) {
		switch(v.getId()) {
			case R.id.picture:
				listener.onTakePicture();
				break;
			case R.id.info:
				listener.onShowInfo();
				break;
		}
	}
}
