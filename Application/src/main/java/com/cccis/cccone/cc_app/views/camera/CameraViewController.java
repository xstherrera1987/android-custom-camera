package com.cccis.cccone.cc_app.views.camera;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.camera2.CameraManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v13.app.ActivityCompat;
import android.widget.Toast;

import com.cccis.cccone.cc_app.R;
import com.cccis.cccone.cc_app.custom_camera.CustomCamera;
import com.cccis.cccone.cc_app.tracer.Tracer;
import com.cccis.cccone.cc_app.views.misc.ConfirmationDialog;
import com.cccis.cccone.cc_app.views.misc.ErrorDialog;

import java.io.File;

public class CameraViewController implements CameraListener, CustomCamera.Delegate {
	public static final String FRAGMENT_DIALOG = "fragment_dialog";

	CameraView view;
	CameraActivity activity;
	CameraViewModel viewModel;
	CustomCamera customCamera;

	/** additional thread for running tasks that shouldn't block the UI */
	HandlerThread mBackgroundThread;

	/** Handler for running tasks in the background. */
	Handler mBackgroundHandler;


	public CameraViewController(CameraActivity cameraActivity, CameraView cameraView) {
		activity = cameraActivity;
		viewModel = new CameraViewModel();

		view = cameraView;
		view.setListener(this);


	}

	//region Lifecycle

	public void onActivityPause() {
		Tracer.traceDebug("CameraVC.onActivityPause");

		customCamera.endCameraSession();
        stopBackgroundThread();
	}

	public void onActivityResume() {
		Tracer.traceDebug("CameraVC.onActivityResume");
		startBackgroundThread();

		customCamera = new CustomCamera(this, (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE), activity, view);
		customCamera.setBackgroundHandler(mBackgroundHandler);
        customCamera.init();

		if (customCamera.tempFile == null) {
			customCamera.tempFile = new File(activity.getExternalFilesDir(null), "pic.jpg");
		}
	}
	//endregion

	//region view callbacks
	@Override public void onTakePicture() {
		Tracer.traceDebug("CameraVC.onTakePicture");
		customCamera.takePicture();
	}

	@Override public void onShowInfo() {
		Tracer.traceDebug("CameraVC.onShowInfo");
		showInfoDialog();
	}
	//endregion

	public void showToast(final String text) {
		Tracer.traceDebug("CameraVC.showToast");
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void requestCameraPermission() {
		Tracer.traceVerbose("CameraVC.requestCameraPermission");

		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
			new ConfirmationDialog().show(activity.getFragmentManager(), FRAGMENT_DIALOG);
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, CustomCamera.REQUEST_CAMERA_PERMISSION);
		}
	}

	@Override public void endActivity() {
		Tracer.traceDebug("CameraVC.endActivity");
		activity.finish();
	}

	@Override public int getOrientation() {
		return activity.getResources().getConfiguration().orientation;
	}

	@Override public int getRotation() {
		return activity.getWindowManager().getDefaultDisplay().getRotation();
	}

	@Override public Point getDisplaySize() {
		Point displaySize = new Point();
		activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
		return displaySize;
	}

	@Override public void showErrorDialog(String message) {
		Tracer.traceDebug("CameraVC.showErrorDialog");
		ErrorDialog.newInstance(message).show(activity.getFragmentManager(), FRAGMENT_DIALOG);
	}

	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		Tracer.traceVerbose("CameraVC.onRequestPermissionsResult");

		if (requestCode == CustomCamera.REQUEST_CAMERA_PERMISSION) {
			if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				ErrorDialog.newInstance(activity.getString(R.string.request_permission))
						.show(activity.getFragmentManager(), FRAGMENT_DIALOG);
			}
		} else {
			activity.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	/** start background thread and its Handler */
	private void startBackgroundThread() {
		Tracer.traceVerbose("CameraVC.startBackgroundThread");

		mBackgroundThread = new HandlerThread("CameraBackground");
		mBackgroundThread.start();
		mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
	}

	/** stop the background thread and its Handler */
	public void stopBackgroundThread() {
		Tracer.traceVerbose("CameraVC.stopBackgroundThread");
		mBackgroundThread.quitSafely();
		try {
			mBackgroundThread.join();
			mBackgroundThread = null;
			mBackgroundHandler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void showInfoDialog() {
		Tracer.traceDebug("CameraVC.showInfoDialog");
		new AlertDialog.Builder(activity)
				.setMessage(R.string.intro_message)
				.setPositiveButton(android.R.string.ok, null)
				.show();
	}
}
