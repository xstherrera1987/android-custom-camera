package com.cccis.cccone.cc_app.object_model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.lang.ref.WeakReference;

public abstract class BaseModel {
	private WeakReference<ModelListener> modelListener;
	protected PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	public void fireOnModelUpdated( String propertyName ) {
		if ( modelListener != null ) {
			ModelListener ref = modelListener.get();
			if ( ref != null ) {
				ref.onModelUpdated(this, propertyName);
			}
		}

		propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, propertyName, null, null));
	}

	public void setListener(ModelListener listener ) {
		modelListener = new WeakReference<>(listener);
	}
}
