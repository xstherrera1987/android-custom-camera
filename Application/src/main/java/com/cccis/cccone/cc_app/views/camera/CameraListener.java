package com.cccis.cccone.cc_app.views.camera;

public interface CameraListener {
	void onTakePicture();
	void onShowInfo();
}
