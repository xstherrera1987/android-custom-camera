package com.cccis.cccone.cc_app.views.camera;

import android.app.Activity;
import android.os.Bundle;

import com.cccis.cccone.cc_app.R;
import com.cccis.cccone.cc_app.tracer.Tracer;

public class CameraActivity extends Activity {
    CameraViewController viewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Tracer.traceVerbose("CameraAct.onResume");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        viewController = new CameraViewController(this, (CameraView) findViewById(R.id.camera_view));
    }

    @Override
    public void onResume() {
        Tracer.traceVerbose("CameraAct.onResume");
        super.onResume();
        viewController.onActivityResume();
    }

    @Override
    public void onPause() {
        Tracer.traceVerbose("CameraAct.onPause");
        viewController.onActivityPause();
        super.onPause();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Tracer.traceVerbose("CameraAct.onRequestPermissionsResult");
        viewController.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
