package com.cccis.cccone.cc_app.object_model;

public interface ModelListener {
	void onModelUpdated( BaseModel model, String propertyName );
}
