package com.cccis.cccone.cc_app.views.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cccis.cccone.cc_app.views.camera.CameraActivity;
import com.cccis.cccone.cc_app.R;

public class MainActivity extends Activity implements View.OnClickListener {

	Button cameraBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		cameraBtn = (Button) findViewById(R.id.camera_btn);
		cameraBtn.setOnClickListener(this);
	}

	@Override public void onClick(View v) {
		if (v.getId() == R.id.camera_btn) {
			startActivity(new Intent(this, CameraActivity.class));
		}
	}
}
