package com.cccis.cccone.cc_app.custom_camera;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomCameraPhotoCaptureSession {
	Context context;
	public final List<File> files = new ArrayList<>();

	public int currentPhotoIndex = 0;

	public CustomCameraPhotoCaptureSession(Context context) {
		this.context = context;
	}

	public File newFile(String filename) {
		return new File(context.getExternalFilesDir(null), filename);
	}
	public File newFile() {
		return new File(context.getExternalFilesDir(null), newGuid());
	}

	private String newGuid() {
		return UUID.randomUUID().toString();
	}
}
