package com.cccis.cccone.cc_app.custom_camera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;

import com.cccis.cccone.cc_app.domain.CompareSizesByArea;
import com.cccis.cccone.cc_app.domain.ImageSaver;
import com.cccis.cccone.cc_app.tracer.Tracer;
import com.cccis.cccone.cc_app.views.camera.CameraView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class CustomCamera {
	public static final SparseIntArray ORIENTATIONS = new SparseIntArray();
	static {
		CustomCamera.ORIENTATIONS.append(Surface.ROTATION_0, 90);
		CustomCamera.ORIENTATIONS.append(Surface.ROTATION_90, 0);
		CustomCamera.ORIENTATIONS.append(Surface.ROTATION_180, 270);
		CustomCamera.ORIENTATIONS.append(Surface.ROTATION_270, 180);
	}

	// Camera state: Showing camera preview.
	public static final int STATE_PREVIEW = 0;
	// Camera state: Waiting for the focus to be locked.
	public static final int STATE_WAITING_LOCK = 1;
	// Camera state: Waiting for the exposure to be precapture state.
	public static final int STATE_WAITING_PRECAPTURE = 2;
	// Camera state: Waiting for the exposure state to be something other than precapture.
	public static final int STATE_WAITING_NON_PRECAPTURE = 3;
	// Camera state: Picture was taken.
	public static final int STATE_PICTURE_TAKEN = 4;

	public static final int REQUEST_CAMERA_PERMISSION = 1;

	// Max preview width that is guaranteed by Camera2 API
	public static final int MAX_PREVIEW_WIDTH = 1920;
	// Max preview height that is guaranteed by Camera2 API
	public static final int MAX_PREVIEW_HEIGHT = 1080;

	/**
	 * ID of the current {@link CameraDevice}.
	 */
	public String cameraId;


	/**
	 * A reference to the opened {@link CameraDevice}.
	 */
	public CameraDevice cameraDevice;

	/**
	 * A {@link Semaphore} to prevent the app from exiting before closing the camera.
	 */
	public Semaphore cameraOpenCloseLock = new Semaphore(1);

	/**
	 * The current state of camera state for taking pictures.
	 *
	 * @see #mCaptureCallback
	 */
	public int cameraState = CustomCamera.STATE_PREVIEW;

	/**
	 * Whether the current camera device supports Flash or not.
	 */
	public boolean flashSupported;

	/**
	 * Orientation of the camera sensor
	 */
	public int sensorOrientation;

	/**
	 * {@link CaptureRequest.Builder} for the camera preview
	 */
	public CaptureRequest.Builder previewRequestBuilder;

	/**
	 * {@link CaptureRequest} generated by {@link #previewRequestBuilder}
	 */
	public CaptureRequest previewRequest;

	/**
	 * The {@link android.util.Size} of camera preview.
	 */
	public Size previewSize;

	/**
	 * An {@link ImageReader} that handles still image capture.
	 */
	public ImageReader imageReader;

	/**
	 * This is the output file for our picture.
	 */
	public File tempFile;

	CustomCameraPhotoCaptureSession customCaptureSession;

	// delegates / listeners
	Delegate delegate;
	Handler backgroundHandler;
	CameraView cameraView;
	CameraCaptureSession captureSession;

	// Injects
	CameraManager cameraManager;
	Context context;


	public CustomCamera(Delegate delegate, CameraManager cameraManager, Context context, CameraView cameraView) {
		this.delegate = delegate;
		this.cameraManager = cameraManager;
		this.context = context;
		this.cameraView = cameraView;

		this.customCaptureSession = new CustomCameraPhotoCaptureSession(this.context);
	}

	public void setBackgroundHandler(Handler handler) {
		this.backgroundHandler = handler;
	}

	/**
	 * Retrieves the JPEG orientation from the specified screen rotation.
	 *
	 * @param rotation The screen rotation.
	 * @return The JPEG orientation (one of 0, 90, 270, and 360)
	 */
	public int getOrientation(int rotation) {
		Tracer.traceDebug("CustomCamera.chooseOptimalSize");

		// Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
		// We have to take that into account and rotate JPEG properly.
		// For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
		// For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
		return (CustomCamera.ORIENTATIONS.get(rotation) + sensorOrientation + 270) % 360;
	}

	/**
	 * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
	 * is at least as large as the respective texture view size, and that is at most as large as the
	 * respective max size, and whose aspect ratio matches with the specified value. If such size
	 * doesn't exist, choose the largest one that is at most as large as the respective max size,
	 * and whose aspect ratio matches with the specified value.
	 *
	 * @param choices           The list of sizes that the camera supports for the intended output
	 *                          class
	 * @param textureViewWidth  The width of the texture view relative to sensor coordinate
	 * @param textureViewHeight The height of the texture view relative to sensor coordinate
	 * @param maxWidth          The maximum width that can be chosen
	 * @param maxHeight         The maximum height that can be chosen
	 * @param aspectRatio       The aspect ratio
	 * @return The optimal {@code Size}, or an arbitrary one if none were big enough
	 */
	public static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
										  int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {
		Tracer.traceDebug("CustomCamera.chooseOptimalSize");

		// Collect the supported resolutions that are at least as big as the preview Surface
		List<Size> bigEnough = new ArrayList<>();
		// Collect the supported resolutions that are smaller than the preview Surface
		List<Size> notBigEnough = new ArrayList<>();
		int w = aspectRatio.getWidth();
		int h = aspectRatio.getHeight();
		for (Size option : choices) {
			if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
					option.getHeight() == option.getWidth() * h / w) {
				if (option.getWidth() >= textureViewWidth &&
						option.getHeight() >= textureViewHeight) {
					bigEnough.add(option);
				} else {
					notBigEnough.add(option);
				}
			}
		}

		// Pick the smallest of those big enough. If there is no one big enough, pick the
		// largest of those not big enough.
		if (bigEnough.size() > 0) {
			return Collections.min(bigEnough, new CompareSizesByArea());
		} else if (notBigEnough.size() > 0) {
			return Collections.max(notBigEnough, new CompareSizesByArea());
		} else {
			Tracer.traceError("Couldn't find any suitable preview size");
			return choices[0];
		}
	}

	/**  Opens the camera specified by mCameraId */
	public void openCamera(int width, int height) {
		Tracer.traceVerbose("CustomCamera.openCamera");

		if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			delegate.requestCameraPermission();
			return;
		}

		setUpCameraOutputs(width, height);
		configureTransform(width, height);

		try {
			if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
				throw new RuntimeException("Time out waiting to lock camera opening.");
			}

			cameraManager.openCamera(cameraId, cameraStateCallback, backgroundHandler);

		} catch (CameraAccessException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
		}
	}

	public void endCameraSession() {
		Tracer.traceVerbose("CustomCamera.endCameraSession");
		closeCamera();
		closeCameraPreviewSession();
	}

	public void closeCamera() {
		Tracer.traceVerbose("CustomCamera.closeCamera");
		try {
			cameraOpenCloseLock.acquire();

			if (null != cameraDevice) {
				cameraDevice.close();
				cameraDevice = null;
			}
			if (null != imageReader) {
				imageReader.close();
				imageReader = null;
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
		} finally {
			cameraOpenCloseLock.release();
		}
	}

	public void init() {
		Tracer.traceVerbose("CustomCamera.init");
		cameraView.textureView.setSurfaceTextureListener(surfaceTextureListener);
	}

	public void setUpCameraOutputs(int width, int height) {
		Tracer.traceVerbose("CustomCamera.setUpCameraOutputs");

		try {
			for (String cameraId : cameraManager.getCameraIdList()) {
				CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);

				// We don't use a front facing camera in this sample.
				Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
				if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
					continue;
				}

				StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
				if (map == null) {
					continue;
				}

				// For still image captures, we use the largest available size.
				Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());
				imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, /*maxImages*/2);
				imageReader.setOnImageAvailableListener(onImageAvailableListener, backgroundHandler);

				// Find out if we need to swap dimension to get the preview size relative to sensor
				// coordinate.
				int displayRotation = delegate.getRotation();

				//noinspection ConstantConditions
				sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);

				boolean swappedDimensions = false;
				switch (displayRotation) {
					case Surface.ROTATION_0:
					case Surface.ROTATION_180:
						if (sensorOrientation == 90 || sensorOrientation == 270) {
							swappedDimensions = true;
						}
						break;
					case Surface.ROTATION_90:
					case Surface.ROTATION_270:
						if (sensorOrientation == 0 || sensorOrientation == 180) {
							swappedDimensions = true;
						}
						break;
					default:
						Tracer.traceError("Display rotation is invalid: " + displayRotation);
				}

				Point displaySize = delegate.getDisplaySize();

				int rotatedPreviewWidth = width;
				int rotatedPreviewHeight = height;
				int maxPreviewWidth = displaySize.x;
				int maxPreviewHeight = displaySize.y;

				if (swappedDimensions) {
					rotatedPreviewWidth = height;
					rotatedPreviewHeight = width;
					maxPreviewWidth = displaySize.y;
					maxPreviewHeight = displaySize.x;
				}

				if (maxPreviewWidth > CustomCamera.MAX_PREVIEW_WIDTH) {
					maxPreviewWidth = CustomCamera.MAX_PREVIEW_WIDTH;
				}

				if (maxPreviewHeight > CustomCamera.MAX_PREVIEW_HEIGHT) {
					maxPreviewHeight = CustomCamera.MAX_PREVIEW_HEIGHT;
				}

				// Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
				// bus' bandwidth limitation, resulting in gorgeous previews but the storage of
				// garbage capture data.
				previewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
						rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
						maxPreviewHeight, largest);

				// We fit the aspect ratio of TextureView to the size of preview we picked.
				int orientation = delegate.getOrientation();

				if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
					cameraView.textureView.setAspectRatio(
							previewSize.getWidth(), previewSize.getHeight());
				} else {
					cameraView.textureView.setAspectRatio(
							previewSize.getHeight(), previewSize.getWidth());
				}

				// Check if the flash is supported.
				Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
				flashSupported = available == null ? false : available;

				Tracer.traceVerbose("got camera-id: " + cameraId);
				this.cameraId = cameraId;
				return;
			}
		} catch (CameraAccessException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			// Currently an NPE is thrown when the Camera2API is used but not supported on the
			// device this code runs.
			delegate.showErrorDialog("This device doesn't support Camera2 API.");

		}
	}

	/**
	 * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
	 * This method should be called after the camera preview size is determined in
	 * setUpCameraOutputs and also the size of `mTextureView` is fixed.
	 *
	 * @param viewWidth  The width of `mTextureView`
	 * @param viewHeight The height of `mTextureView`
	 */
	public void configureTransform(int viewWidth, int viewHeight) {
		Tracer.traceVerbose("CustomCamera.configureTransform");

		if (null == cameraView.textureView || null == previewSize) {
			return;
		}

		int rotation = delegate.getRotation();
		Matrix matrix = new Matrix();
		RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
		RectF bufferRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());

		float centerX = viewRect.centerX();
		float centerY = viewRect.centerY();

		if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
			bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
			matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);

			float scale = Math.max(
					(float) viewHeight / previewSize.getHeight(),
					(float) viewWidth / previewSize.getWidth());

			matrix.postScale(scale, scale, centerX, centerY);
			matrix.postRotate(90 * (rotation - 2), centerX, centerY);
		} else if (Surface.ROTATION_180 == rotation) {
			matrix.postRotate(180, centerX, centerY);
		}

		cameraView.textureView.setTransform(matrix);
	}

	/**
	 * Creates a new {@link CameraCaptureSession} for camera preview.
	 */
	private void createCameraPreviewSession() {
		Tracer.traceVerbose("CustomCamera.createCameraPreviewSession");
		try {
			SurfaceTexture texture = cameraView.textureView.getSurfaceTexture();
			assert texture != null;
			assert cameraDevice != null;

			// We configure the size of default buffer to be the size of camera preview we want.
			texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());

			// This is the output Surface we need to start preview.
			Surface surface = new Surface(texture);

			// We set up a CaptureRequest.Builder with the output Surface.
			previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
			previewRequestBuilder.addTarget(surface);

			// Here, we create a CameraCaptureSession for camera preview.
			cameraDevice.createCaptureSession(Arrays.asList(surface, imageReader.getSurface()),
					new CameraCaptureSession.StateCallback() {

						@Override
						public void onConfigured(CameraCaptureSession cameraCaptureSession) {
							Tracer.traceVerbose("CameraCaptureSession.StateCallback.onConfigured");

							// The camera is already closed
							if (null == cameraDevice) {
								return;
							}

							// When the session is ready, we start displaying the preview.
							captureSession = cameraCaptureSession;
							try {
								// Auto focus should be continuous for camera preview.
								previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
										CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
								// Flash is automatically enabled when necessary.
								setAutoFlash(previewRequestBuilder);

								// Finally, we start displaying the camera preview.
								previewRequest = previewRequestBuilder.build();
								captureSession.setRepeatingRequest(previewRequest, mCaptureCallback, backgroundHandler);
							} catch (CameraAccessException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
							Tracer.traceVerbose("CameraCaptureSession.StateCallback.onConfigureFailed");
							delegate.showToast("Failed");
						}
					}, null
			);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	public void closeCameraPreviewSession() {
		Tracer.traceVerbose("CustomCamera.closeCameraPreviewSession");
		if (null != captureSession) {
			captureSession.close();
			captureSession = null;
		}
	}

	/** callback object for ImageReader. "onImageAvailable" will be called when a  still image is ready to be saved */
	final ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
		@Override
		public void onImageAvailable(ImageReader reader) {
			Tracer.traceVerbose("ImageReader.OnImageAvailableListener.onImageAvailable");
			backgroundHandler.post(new ImageSaver(reader.acquireNextImage(), tempFile));
		}
	};

	/** TextureView.SurfaceTextureListener} handles several lifecycle events on a TextureView */
	final TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
			Tracer.traceVerbose("TextureView.SurfaceTextureListener.onSurfaceTextureAvailable");
			openCamera(width, height);
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
			Tracer.traceVerbose("TextureView.SurfaceTextureListener.onSurfaceTextureSizeChanged");
			configureTransform(width, height);
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
			Tracer.traceVerbose("TextureView.SurfaceTextureListener.onSurfaceTextureDestroyed");
			return true;
		}
		@Override public void onSurfaceTextureUpdated(SurfaceTexture texture) {
			Tracer.traceVerbose("TextureView.SurfaceTextureListener.onSurfaceTextureUpdated");
		}
	};

	/**
	 * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
	 */
	private final CameraDevice.StateCallback cameraStateCallback = new CameraDevice.StateCallback() {

		@Override
		public void onOpened(CameraDevice cameraDevice) {
			Tracer.traceVerbose("CameraDevice.StateCallback.onOpened");
			// This method is called when the camera is opened.  We start camera preview here.
			cameraOpenCloseLock.release();
			CustomCamera.this.cameraDevice = cameraDevice;
			createCameraPreviewSession();
		}

		@Override
		public void onDisconnected(CameraDevice cameraDevice) {
			Tracer.traceVerbose("CameraDevice.StateCallback.onDisconnected");
			cameraOpenCloseLock.release();
			cameraDevice.close();
			CustomCamera.this.cameraDevice = null;
		}

		@Override
		public void onError(CameraDevice cameraDevice, int error) {
			Tracer.traceVerbose("CameraDevice.StateCallback.onError");
			cameraOpenCloseLock.release();
			cameraDevice.close();
			CustomCamera.this.cameraDevice = null;
			Tracer.traceError("error-code: " + error);
			delegate.endActivity();
		}
	};

	/**
	 * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
	 */
	private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {

		private void process(CaptureResult result) {
			Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.process");

			switch (cameraState) {
				case CustomCamera.STATE_PREVIEW: {
					Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.process STATE_PREVIEW");
					// We have nothing to do when the camera preview is working normally.
					break;
				}
				case CustomCamera.STATE_WAITING_LOCK: {
					Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.process STATE_WAITING_LOCK");
					Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
					if (afState == null) {
						captureStillPicture();
					} else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
							CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
						// CONTROL_AE_STATE can be null on some devices
						Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
						if (aeState == null ||
								aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
							cameraState = CustomCamera.STATE_PICTURE_TAKEN;
							captureStillPicture();
						} else {
							runPrecaptureSequence();
						}
					}
					break;
				}
				case CustomCamera.STATE_WAITING_PRECAPTURE: {
					Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.process STATE_WAITING_PRECAPTURE");
					// CONTROL_AE_STATE can be null on some devices
					Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
					if (aeState == null ||
							aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
							aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
						cameraState = CustomCamera.STATE_WAITING_NON_PRECAPTURE;
					}
					break;
				}
				case CustomCamera.STATE_WAITING_NON_PRECAPTURE: {
					Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.process STATE_WAITING_NON_PRECAPTURE");

					// CONTROL_AE_STATE can be null on some devices
					Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
					if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
						cameraState = CustomCamera.STATE_PICTURE_TAKEN;
						captureStillPicture();
					}
					break;
				}
			}
		}

		/**
		 * Run the precapture sequence for capturing a still image. This method should be called when
		 * we get a response in captureCallback} from #lockFocus()
		 */
		private void runPrecaptureSequence() {
			Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.runPrecaptureSequence");

			try {
				// This is how to tell the camera to trigger.
				previewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
						CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
				// Tell #mCaptureCallback to wait for the precapture sequence to be set.
				cameraState = CustomCamera.STATE_WAITING_PRECAPTURE;
				captureSession.capture(previewRequestBuilder.build(), mCaptureCallback, backgroundHandler);
			} catch (CameraAccessException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onCaptureProgressed(CameraCaptureSession session, CaptureRequest request, CaptureResult partialResult) {
			Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.onCaptureProgressed");
			process(partialResult);
		}

		@Override
		public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
			Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.onCaptureCompleted");
			process(result);
		}
	};

	/** Capture a still picture. This method should be called when we get a response in mCaptureCallback} from both #lockFocus() */
	public void captureStillPicture() {
		Tracer.traceVerbose("CustomCamera.captureStillPicture");
		try {
			if (null == cameraDevice) {
				return;
			}

			// This is the CaptureRequest.Builder that we use to take a picture.
			final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
			captureBuilder.addTarget(imageReader.getSurface());

			// Use the same AE and AF modes as the preview.
			captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
			setAutoFlash(captureBuilder);

			// Orientation
			int rotation = delegate.getRotation();
			captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));

			CameraCaptureSession.CaptureCallback captureCallback
					= new CameraCaptureSession.CaptureCallback() {

				@Override
				public void onCaptureCompleted(CameraCaptureSession session,
											   CaptureRequest request,
											   TotalCaptureResult result) {
					Tracer.traceVerbose("CameraCaptureSession.CaptureCallback.onCaptureCompleted");

					delegate.showToast("Saved: " + tempFile.toString());
					Tracer.traceVerbose(tempFile.toString());
					unlockFocus();
				}
			};

			captureSession.stopRepeating();
			captureSession.capture(captureBuilder.build(), captureCallback, null);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	public void setAutoFlash(CaptureRequest.Builder requestBuilder) {
		Tracer.traceVerbose("CustomCamera.setAutoFlash");
		if (flashSupported) {
			requestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
		}
	}

	/**
	 * Unlock the focus. This method should be called when still image capture sequence is
	 * finished.
	 */
	public void unlockFocus() {
		Tracer.traceVerbose("CustomCamera.unlockFocus");
		try {
			// Reset the auto-focus trigger
			previewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);

			setAutoFlash(previewRequestBuilder);

			captureSession.capture(previewRequestBuilder.build(), mCaptureCallback, backgroundHandler);

			// After this, the camera will go back to the normal state of preview.
			cameraState = CustomCamera.STATE_PREVIEW;
			captureSession.setRepeatingRequest(previewRequest, mCaptureCallback, backgroundHandler);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lock the focus as the first step for a still image capture.
	 */
	public void lockFocus() {
		Tracer.traceVerbose("CustomCamera.lockFocus");
		try {
			// This is how to tell the camera to lock focus.
			previewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);

			// Tell #mCaptureCallback to wait for the lock.
			cameraState = CustomCamera.STATE_WAITING_LOCK;

			captureSession.capture(previewRequestBuilder.build(), mCaptureCallback, backgroundHandler);
		} catch (CameraAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initiate a still image capture.
	 */
	public void takePicture() {
		Tracer.traceVerbose("CustomCamera.takePicture");
		lockFocus();
	}

	public interface Delegate {
		void requestCameraPermission();
		void endActivity();

		// NOTE: these will come from services/util
		int getOrientation();
		int getRotation();
		Point getDisplaySize();

		// NOTE: also from a service
		void showErrorDialog(String message);
		void showToast(String message);
	}
}
