# TODO

### Compat
* consider refactoring logic/data into PhotoViewModel (pull from RF-Mobile)
* implement counter
* investigate crash below
* generate random file guid
	+ temp solution, swap out with RF-Mobile solution once integrated
* licensing: determine quantity of code re-use / copy

## Errors
* sleep then wake device causes this error:
```
08-22 15:28:49.983 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraAct.onPause
08-22 15:28:49.983 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.closeCamera
08-22 15:28:49.984 6629-6629/com.cccis.cccone.custom_camera I/RequestQueue: Repeating capture request cancelled.
08-22 15:28:50.273 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.stopBackgroundThread
08-22 15:28:51.864 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.openCamera
08-22 15:28:51.865 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.setUpCameraOutputs
08-22 15:28:51.877 6629-6629/com.cccis.cccone.custom_camera D/CustomCamera: CustomCamera.chooseOptimalSize
08-22 15:28:51.877 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.configureTransform
08-22 15:28:51.884 6629-6629/com.cccis.cccone.custom_camera I/CameraManager: Using legacy camera HAL.
08-22 15:28:52.086 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraAct.onResume
08-22 15:28:52.087 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.startBackgroundThread
08-22 15:28:52.088 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.openCamera
08-22 15:28:52.089 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.setUpCameraOutputs
08-22 15:28:52.098 6629-6629/com.cccis.cccone.custom_camera D/CustomCamera: CustomCamera.chooseOptimalSize
08-22 15:28:52.099 6629-6629/com.cccis.cccone.custom_camera V/CustomCamera: CameraVC.configureTransform
08-22 15:28:54.600 6629-6629/com.cccis.cccone.custom_camera D/AndroidRuntime: Shutting down VM


                                                                              --------- beginning of crash
08-22 15:28:54.601 6629-6629/com.cccis.cccone.custom_camera E/AndroidRuntime: FATAL EXCEPTION: main
                                                                              Process: com.cccis.cccone.custom_camera, PID: 6629
                                                                              java.lang.RuntimeException: Unable to resume activity {com.cccis.cccone.custom_camera/com.cccis.cccone.custom_camera.CameraActivity}: java.lang.RuntimeException: Time out waiting to lock camera opening.
                                                                                  at android.app.ActivityThread.performResumeActivity(ActivityThread.java:3421)
                                                                                  at android.app.ActivityThread.handleResumeActivity(ActivityThread.java:3461)
                                                                                  at android.app.ActivityThread$H.handleMessage(ActivityThread.java:1523)
                                                                                  at android.os.Handler.dispatchMessage(Handler.java:102)
                                                                                  at android.os.Looper.loop(Looper.java:154)
                                                                                  at android.app.ActivityThread.main(ActivityThread.java:6123)
                                                                                  at java.lang.reflect.Method.invoke(Native Method)
                                                                                  at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:867)
                                                                                  at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:757)
                                                                               Caused by: java.lang.RuntimeException: Time out waiting to lock camera opening.
                                                                                  at com.cccis.cccone.custom_camera.CameraViewController.openCamera(CameraViewController.java:430)
                                                                                  at com.cccis.cccone.custom_camera.CameraViewController.onActivityResume(CameraViewController.java:101)
                                                                                  at com.cccis.cccone.custom_camera.CameraActivity.onResume(CameraActivity.java:25)
                                                                                  at android.app.Instrumentation.callActivityOnResume(Instrumentation.java:1291)
                                                                                  at android.app.Activity.performResume(Activity.java:6776)
                                                                                  at android.app.ActivityThread.performResumeActivity(ActivityThread.java:3398)
                                                                                  at android.app.ActivityThread.handleResumeActivity(ActivityThread.java:3461) 
                                                                                  at android.app.ActivityThread$H.handleMessage(ActivityThread.java:1523) 
                                                                                  at android.os.Handler.dispatchMessage(Handler.java:102) 
                                                                                  at android.os.Looper.loop(Looper.java:154) 
                                                                                  at android.app.ActivityThread.main(ActivityThread.java:6123) 
                                                                                  at java.lang.reflect.Method.invoke(Native Method) 
                                                                                  at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:867) 
                                                                                  at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:757) 

```
